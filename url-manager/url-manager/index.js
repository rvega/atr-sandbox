var oldStrategy = require('./old-url-strategy.js');
var newStrategy = require('./new-url-strategy.js');

var urlManager = {};

urlManager.resolve = function(req, res, next){
  var url = req.url;

  // if new url
  if(newStrategy.looksGood(url)){
    newStrategy.resolve(req, res, next); 
    return;
  }

  // If old url
  if(oldStrategy.looksGood(url)){
    // If germany, redirect to new
    var urlParams = oldStrategy.parse(url);
    if(urlParams.country == 'de'){
      var newUrl = newStrategy.generate(urlParams);
      urlManager.redirect(newUrl);
      return;
    }
    // If not germany, resolve old
    else{
      oldStrategy.resolve(req, res, next);
      return;
    }
  }

  // not a valid url?
  else{
    // dont know what to do, let it go, maybe someone else 
    // (other middleware) will take care
    next();
  }

}

urlManager.generate = function(params) {
  if(params.country == 'de'){
    return newStrategy.generate(params);
  }
  else{
    return oldStrategy.generate(params);
  }
}

urlManager.redirect(url){
  // 301
}

module.exports = urlManager;
