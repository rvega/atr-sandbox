var oldURLStrategy = {};

oldURLStrategy.looksGood = function(url){
  // returns true if this looks like a old url.
  // else return false
  // maybe use js regular expressions
}

oldURLStrategy.parse = function(url){
  // returns object like the following with all the info
  // we can get from the url. Type and id describe the 
  // resource they actually want to see. The other stuff 
  // is helpful in case of redirects or whatever.
  // { type: city,  id: berlin, country: de,  continent: europe }
}

oldURLStrategy.resolve = function(req, res, next){
  var parsed = oldURLStrategy.parse(req.url);
  // call the appropriate controller so that it can render
  // the page...
}

oldURLStrategy.generate = function(params){
  // Return a url.
  if(params.type == 'city'){ 
    return '/old/' + params.country + '/' + params.city + '/' +params.id;
  }
  else if(params.type == 'country'){
    return //.....
  }
}
